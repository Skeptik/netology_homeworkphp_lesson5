<?php 
$file = file_get_contents(__DIR__."/json/telephone.json");
$telephone = json_decode($file, true);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Телефонная книга</title>
</head>
<body>
	<table align="center" border="5" cellpadding="5">
		<tr>
			<td><strong>Номер</strong></td>
			<td><strong>Имя</strong></td>
			<td><strong>Фамилия</strong></td>
			<td><strong>Адрес</strong></td>
			<td><strong>Телефон</strong></td>
		</tr>
		<?php foreach ($telephone as $key => $value) {?>
		<tr>
			<td><strong><?php echo $key+1; ?></strong></td>
			<td><?php echo $value["firstName"]; ?></td>
			<td><?php echo $value["lastName"]; ?></td>
			<td><?php echo $value["address"]; ?></td>
			<td><?php echo $value["phoneNumber"]; ?></td>
		</tr>
		<?php } ?>
	</table>
</body>
</html>